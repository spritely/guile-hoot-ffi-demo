all: hello.wasm sxml.wasm counter.wasm todo.wasm

hello.wasm: hello.scm
	guild compile-wasm -o hello.wasm hello.scm

sxml.wasm: sxml.scm
	guild compile-wasm -o sxml.wasm sxml.scm

counter.wasm: counter.scm
	guild compile-wasm -o counter.wasm counter.scm

todo.wasm: todo.scm
	guild compile-wasm -o todo.wasm todo.scm

serve: *.wasm
	guile -c '((@ (hoot web-server) serve))'

clean:
	rm *.wasm
